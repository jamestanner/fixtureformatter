# OKA Fixtures Uploader

This is a small app that is was used to convert from a calender stored in a excel spreadsheet and convert it into a format that could be uploaded to the fixtureslive website with their fixtures importer.

This app works only on windows but can easily be modified to work on other platforms.

The app takes a CSV files for the fixtures, teams and location of the matches, parses them all, then prints out a reformatted csv file containing the information that fixtures live needs to add fixitures to their system.

The app was designed with C++11 required as a minimum.